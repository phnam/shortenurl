##Deploy webapp to tomcat server

1.Open Terminal in project(where the pom.xml file is ) and run : 
```bash
mvn package
```
or
```bash
mvn compile war:war
``` 
2.Create a tomcat user in tomcat-user.xml in ../tomcat/conf.
Eg: 
```xml
<role rolename="admin-gui"/>
   <role rolename="manager-gui"/>
   <user username="admin" password="admin" roles="admin-gui,manager-gui"/>
```
3.Copy war file to tomcat, run :
```bash
sudo cp ${PWD}/ShortenUrl/target/ShortenUrl.war /opt/tomcat/webapps/
```

---

##Setup nginx as a reverse proxy for tomcat && Setup ssl self-signed cert for nginx

Automation setup:
Open Terminal in repository and run: 
```bash
sudo bash automation-script
```
To setup manual:
Follow [Configure nginx as reverse proxy for tomcat](https://websiteforstudents.com/configure-nginx-proxy-server-for-tomcat-9-on-ubuntu-16-04-17-10-18-04/)
and 
[Create a self-signed ssl certificate for nginx](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-18-04)


