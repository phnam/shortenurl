package com.namph.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.namph.model.Url;
import com.namph.repository.UrlDao;
import com.namph.utils.GetShortenUrl;
@Service
public class UrlService {
	@Autowired
	UrlDao urlDao;
	
	public String save(String url) {
		try {
			Url u = new Url(GetShortenUrl.getShortenUrl(url), url);
			if(urlDao.getUrl(u.getId()) == null) {
				urlDao.save(u);
			}
			return u.getId();
		} catch (Exception e) {
			return null;
		}
	}
	 
	public String getUrl(String id) {
		try {
			return urlDao.getUrl(id);
		} catch (Exception e) {
		}
		return null;
	}
	
	public boolean checkConnection() {
		try {
			urlDao.connect();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
