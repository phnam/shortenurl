package com.namph.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.springframework.stereotype.Repository;

import com.namph.model.Url;

@Repository
public class UrlDaoImpl implements UrlDao {

	@Override
	public Connection connect() throws Exception {
		String dbFile = System.getProperty("user.home") + "//db//shortenurl.db";
		String url = "jdbc:sqlite:" + dbFile;
		Connection conn = null;
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection(url);
		return conn;
	}

	@Override
	public void save(Url u) throws Exception {
		String sql = "INSERT INTO url (id, url) " + "values (?,?)";
		Connection conn = this.connect(); 
		PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, u.getId());
			pstmt.setString(2, u.getUrl());
			pstmt.executeUpdate();
	}

	
	@Override
	public String getUrl(String id) throws Exception {
		String sql = "SELECT url.url FROM url WHERE id = '" + id +"'";
		Connection conn = this.connect();
		Statement smt = conn.createStatement();
		ResultSet rs = smt.executeQuery(sql);
		while (rs.next()) {
			return rs.getString(1);
		}
		return null;
	}

}
