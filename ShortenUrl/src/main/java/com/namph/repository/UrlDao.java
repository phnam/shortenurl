package com.namph.repository;

import java.sql.Connection;

import com.namph.model.Url;

public interface UrlDao {
	 public Connection connect() throws Exception;
	 
	 public void save(Url u) throws Exception;
	 
	 public String getUrl(String id) throws Exception;
}
