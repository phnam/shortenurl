package com.namph.model;

public class StringResponse {
	private boolean access_db_ok;
	
	public StringResponse(boolean access_db_ok) {
		super();
		this.access_db_ok = access_db_ok;
	}

	public boolean isAccess_db_ok() {
		return access_db_ok;
	}

	public void setAccess_db_ok(boolean access_db_ok) {
		this.access_db_ok = access_db_ok;
	}
	
}
