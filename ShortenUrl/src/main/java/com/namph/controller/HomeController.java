package com.namph.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.namph.model.StringResponse;
import com.namph.service.UrlService;

@Controller
public class HomeController {
	@Autowired
	private UrlService urlService;

	@RequestMapping("/")
	public String home(Model model) {
		return "index";
	}

	@RequestMapping("/{url}")
	public RedirectView home(Model model, @PathVariable("url") String url) {
		RedirectView redirectView = new RedirectView();
		redirectView.setUrl(urlService.getUrl(url));
		return redirectView;
	}

	@RequestMapping("save")
	public @ResponseBody String saveUrl(Model model, @RequestParam(value = "url") String url) {
		String response = urlService.save(url);
		return response;
	}
	
	@RequestMapping(value = "health", produces = "application/json")
	public @ResponseBody StringResponse health() {
		return new StringResponse(urlService.checkConnection());
	}
}
