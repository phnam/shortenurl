<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
	<h2>Enter the url</h2>
	<input type="text" id ="long-url">
	<button id = "btnGet">Get shorten url</button>
	<div class="short-url">
		Short URL: <a id="a" href="<%=request.getContextPath()%>"></a>
	</div>
	<br>
	<script>
	$("#btnGet").click(function(){
		console.log($("#long-url").val());
		$.get({
			url : "save",
			data : {
				"url" : $("#long-url").val(),
			},
			success : function(html) {
				console.log(html);
				$("#a").html(html);
				$("#a").attr("href", $("#a").attr("href")+"/" + html);
			},
		});
	});	
	</script>
</body>
</html>
