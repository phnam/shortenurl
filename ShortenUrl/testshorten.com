server {
	listen 443 ssl;
	listen [::]:443 ssl;
	ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
	ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
	include snippets/self-signed.conf;
	include snippets/ssl-params.conf;	

	server_name shortenurl.com www.shortenurl.com;

        root /var/www/example.com/html;
        index index.html index.htm index.nginx-debian.html;
	        
	location / {
                try_files $uri $uri/ =404;
		proxy_pass http://127.0.0.1:8080;
        }

}
server {
    listen 80;
    listen [::]:80;
 
    server_name shortenurl.com www.shortenurl.com;

    proxy_redirect           off;
    proxy_set_header         X-Real-IP $remote_addr;
    proxy_set_header         X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header         Host $http_host;
    
    location / {
            proxy_pass http://127.0.0.1:8080;
        }

    return 302 https://$server_name$request_uri;
}
