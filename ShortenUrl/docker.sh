#! /bin/sh

echo "---SETTING MAVEN PROXY---"

echo "Enter username:"
read username

echo "Enter password:"
read -s password

file=settings.xml

sed -i -e "s/user_name/$username/g" \
-e "s/pass_word/$password/g" $file

docker build -t url-image .
docker run -d -p 8080:8080 --name url-container url-image
